import Foundation

extension Array where Element:Comparable
{
 	 mutating func mergeSort(Left:Int, Right:Int)->Array {

		guard Left == Right else {
  			var mid = (Left + Right) / 2 

  			mergeSort(Left:Left, Right:mid)
 			mergeSort(Left:mid + 1, Right:Right)
  			var i=Left  
 	 		var j = mid + 1 
  			var tmp:[Element]=[] 
  		
  			for step in 0..<(Right - Left + 1) {
    			if (j > Right) || ((i <= mid) && (self[i] < self[j])) {
     				tmp.append( self[i])
      				i+=1
    			}
   				else {
      				tmp.append( self[j])
      				j+=1
    			}
  			}
 
  			for  step in 0..<(Right - Left + 1){
   				self[Left + step] = tmp[step]
  			}

  			return self
		}
		return []
	}
}

 var a:[Int]=[4,1,6,5]

a=a.mergeSort(Left: 0,Right:3)
print(a)